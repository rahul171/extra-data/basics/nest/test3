import {Column, Entity, PrimaryGeneratedColumn, TableInheritance} from "typeorm";
import {SourceType} from "../source-type";

@Entity()
@TableInheritance({ column: { type: 'enum', name: 'type', enum: SourceType } })
export class Content {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: SourceType,
  })
  type: SourceType;

}

import {ChildEntity, Column} from "typeorm";
import {Content} from "./content.entity";
import {SourceType} from "../source-type";

@ChildEntity(SourceType.RAID)
export class Post extends Content {

  @Column()
  viewCount: number;

}

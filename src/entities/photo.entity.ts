import {ChildEntity, Column} from "typeorm";
import {Content} from "./content.entity";
import {SourceType} from "../source-type";

@ChildEntity(SourceType.ACTIVITY)
export class Photo extends Content {

  @Column()
  size: string;

}

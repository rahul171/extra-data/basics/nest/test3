export enum SourceType {
  RAID = 'raid',
  REPUTATION = 'reputation',
  REDEEM = 'redeem',
  ACTIVITY = 'activity',
  OTHER = 'other',
}

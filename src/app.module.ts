import {Module} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Content} from "./entities/content.entity";
import {Post} from "./entities/post.entity";
import {Photo} from "./entities/photo.entity";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'abcd1234',
      database: 'table-inheritance-test',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Content, Post, Photo]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

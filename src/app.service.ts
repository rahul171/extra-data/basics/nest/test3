import { Injectable } from '@nestjs/common';
import {InjectEntityManager, InjectRepository} from "@nestjs/typeorm";
import {EntityManager, Repository} from "typeorm";
import {Post} from "./entities/post.entity";
import {Photo} from "./entities/photo.entity";

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
    @InjectRepository(Photo)
    private photoRepository: Repository<Photo>,
    // @InjectEntityManager()
    // private entityManager: EntityManager,
  ) {
    const post = new Post();
    post.viewCount = 10;
    postRepository.save(post);

    // const photo = new Photo();
    // photo.size = 'large';
    // photoRepository.save(photo);
  }

  getHello(): string {
    return 'Hello World!';
  }
}
